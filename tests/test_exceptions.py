from django.template import Context, Template
from django.test import SimpleTestCase

from .django_settings import *

from django_component.arg_tag import (
    IllegalArgParentException,
    ArgAlreadyDefinedException,
)


class TemplateTagTest(SimpleTestCase):
    def _test_template(self, template_str, expectedHTML):
        template = Template(template_str)
        rendered = template.render(Context())
        self.assertHTMLEqual(rendered, expectedHTML)

    def test_IllegalArgParentException(self):
        template = Template("{% load test_components %}{% arg arg_name %}{% endarg %}")
        with self.assertRaises(IllegalArgParentException):
            template.render(Context())

    def test_ArgAlreadyDefinedException(self):
        template = Template(
            """
            {% Arg %}
                {% arg title %}First{% endarg %}
                {% arg title %}Second{% endarg %}
            {% /Arg %}
        """
        )
        with self.assertRaises(ArgAlreadyDefinedException):
            template.render(Context())

    def test_ArgAlreadyDefinedException2(self):
        template = Template(
            """
            {% Arg title="First" %}
                {% arg title %}Second{% endarg %}
            {% /Arg %}
        """
        )
        with self.assertRaises(ArgAlreadyDefinedException):
            template.render(Context())
