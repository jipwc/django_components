import django
from django.conf import settings

if not settings.configured:
    settings.configure(
        INSTALLED_APPS=("django_component", "tests"),
        TEMPLATES=[
            {
                "BACKEND": "django.template.backends.django.DjangoTemplates",
                "DIRS": ["tests/templates/"],
                "OPTIONS": {
                    "builtins": [
                        "django_component.templatetags",
                        "tests.templatetags.test_components",
                    ]
                },
            }
        ],
    )

    django.setup()
