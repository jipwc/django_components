from django.template import Context, Template
from django.test import SimpleTestCase

from .django_settings import *


class TemplateTagTest(SimpleTestCase):
    def _test_template(self, template_str, expectedHTML):
        template = Template(template_str)
        rendered = template.render(Context())
        self.assertHTMLEqual(rendered, expectedHTML)

    def test_component_children(self):
        self._test_template(
            """
                {% load test_components %}
                {% Children %}
                    <p>Content</p>
                {% /Children %}
            """,
            """
                <div>
                    <p>Content</p>
                </div>
            """,
        )

    def test_component_context(self):
        self._test_template(
            "{% load test_components %}{% Context/ number=5 %}", "<p>5 * 5 = 25</p>",
        )

    def test_component_media(self):
        self._test_template(
            """
                {% load test_components %}
                {% components_css %}
                {% Media/ %}
                {% components_js %}
            """,
            """
            <link href="media.css" type="text/css" media="all" rel="stylesheet">
            <div></div>
            <script type="text/javascript" src="media.js"></script>
            """,
        )

    def test_component_multiple_media(self):
        self._test_template(
            """
                {% load test_components %}
                {% components_css %}
                {% Media/ %}
                {% MediaExternal/ %}
                {% components_js %}
            """,
            """
            <link href="media.css" type="text/css" media="all" rel="stylesheet">
            <link href="https://example.com/media.css" type="text/css" media="all" rel="stylesheet">
            <div></div>
            <div></div>
            <script type="text/javascript" src="media.js"></script>
            <script type="text/javascript" src="https://example.com/media.js"></script>
            """,
        )

    def test_component_media_after(self):
        self._test_template(
            """
                {% load test_components %}
                {% Media/ %}
                {% components_css %}
                {% components_js %}
            """,
            """
            <div></div>
            <link href="media.css" type="text/css" media="all" rel="stylesheet">
            <script type="text/javascript" src="media.js"></script>
            """,
        )

    def test_component_prop(self):
        self._test_template(
            "{% load test_components %}{% Prop/ prop='test' %}", "<p>test</p>",
        )

    def test_component_arg_tag(self):
        self._test_template(
            """
                {% load test_components %}
                {% Arg %}
                    {% arg title %}
                        <span>Arg</span> title
                    {% endarg %}
                {% /Arg %}
            """,
            """
                <div>
                    <h1><span>Arg</span> title</h1>
                </div>
            """,
        )

    def test_component_nested(self):
        self._test_template(
            """
                {% load test_components %}
                {% components_css %}
                {% Nested/ %}
                {% components_js %}
            """,
            """
            <link href="media.css" type="text/css" media="all" rel="stylesheet">
            <div></div>
            <script type="text/javascript" src="media.js"></script>
            """,
        )
