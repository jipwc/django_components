# django-component changelog

## UNRELEASE

* slots is removed in favor of arg
* Remove use_components tag, and support media tags after components
* All components are both registered both as self-closed and as not sefl-closed

## 0.1.2

* Support nested components
